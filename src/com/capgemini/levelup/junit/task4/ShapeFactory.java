package com.capgemini.levelup.junit.task4;

import com.capgemini.levelup.junit.task4.shape.Circle;
import com.capgemini.levelup.junit.task4.shape.Shape;
import com.capgemini.levelup.junit.task4.shape.Triangle;

public class ShapeFactory{

	
	public Shape getEmployee(String shapeType) {
		
		if (shapeType != null) {
			
			if (shapeType.equalsIgnoreCase("circle")) {
				return new Circle();
				
			}else if (shapeType.equalsIgnoreCase("triangle")) {
				
				return new Triangle();
			}
		}
		throw new IllegalArgumentException("I don't have such shape");
	}
}
