package com.capgemini.levelup.junit.task2.person;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {

	
	private List<Employee> managedEmployees = new ArrayList<> ();

	public List<Employee> getManagedEmployees() {
		return managedEmployees;
	}
	
}
