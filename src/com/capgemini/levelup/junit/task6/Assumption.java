package com.capgemini.levelup.junit.task6;

import java.util.Random;

public class Assumption {

	private boolean serverRunning = new Random().nextBoolean();
	
	
	
	public String generateMessage() {
		if (! serverRunning) {
			throw new RuntimeException("cannot generate answer. Server is not running");
		}
		return "Hello";
	}



	public boolean isServerRunning() {
		return serverRunning;
	}



}
