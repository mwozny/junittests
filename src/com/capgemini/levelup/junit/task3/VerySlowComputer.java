package com.capgemini.levelup.junit.task3;

public class VerySlowComputer {

	
	public String doSomethingWithInput(String input) throws InterruptedException {
		 Thread.sleep(1000); //pauza na 1 sekunde
		 return input;
	}
}
