package com.capgemini.levelup.junit.task1;

public class Hello {
	
	public String hello() {
		return "Hello Junit";
	}
	
	public String helloYou(String name) {
		if (name == null || name.isEmpty()) {
			return "Invalid name.";
		}else {
			return "Hello " + name;
		}
	}
	

}
