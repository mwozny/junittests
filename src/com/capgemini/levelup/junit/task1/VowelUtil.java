package com.capgemini.levelup.junit.task1;

import java.util.Arrays;
import java.util.List;

public class VowelUtil {

	private String[] vowels = { "a", "e", "o", "i", "u" };

	
	
	public boolean containsVowels(String inputText) {
		
		List<String> vowelList = Arrays.asList(vowels);
		
		for (int i = 0; i < inputText.length(); i++) {
			
			char currentCharacter = inputText.charAt(i);
			
			if ( vowelList.contains(currentCharacter)) {
				return true;
			}
		}
		return false;
	}


	public int howManyVowels(String inputText) {
		int numberOfVowels = 0;

		List<String> vowelList = Arrays.asList(vowels);
		
		for (int i = 0; i < inputText.length(); i++) {
			
			char currentCharacter = inputText.charAt(i);
			
			if ( vowelList.contains(currentCharacter)) {
				numberOfVowels++;
			}
		}
		return numberOfVowels;
	}
	

}
