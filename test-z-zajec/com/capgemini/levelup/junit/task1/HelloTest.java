package com.capgemini.levelup.junit.task1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HelloTest {
    
    private Hello hello ; 
    
    @Before
    public void setUp() throws Exception {
        hello = new Hello();
    }
    
    @Test
    public void shouldAlwaysReturnHelloJunit() {
        //given
        String expectedResult = "Hello Junit";
        
        //when
        String actualResult = hello.hello();
        
        //then
        assertNotNull(actualResult);
        assertEquals(expectedResult, actualResult);
    }
    
    @Test
    public void shouldReturnInvalidNameWhenInputEqualsNull() {
        //given
        String input = null;
        String expectedResult = "Invalid name.";
        
        //when
        String actualResult = hello.helloYou(input);
        
        //then
        assertEquals(expectedResult, actualResult);
        
    }
    
    @Test
    public void shouldReturnHelloMarcinWhenInputEqualsMarcin() {
        //given
        String input = "Marcin";
        String expectedResult = "Hello Marcin";
        
        //when
        String actualResult = hello.helloYou(input);
        
        //then
        assertEquals(expectedResult, actualResult);
        
    }
    
    
    @Test
    public void shouldReturnInvalidNameWhenStringIsEmpty() {
        //given
        String input = "";
        String expectedResult = "Invalid name.";
        
        //when
        String actualResult = hello.helloYou(input);
        
        //then
        assertEquals(expectedResult, actualResult);
        
    }
    
    @Test
    public void shouldReturnInvalidNameWhenStringIsBlank() {
        //given
        String input = " ";
        String expectedResult = "Hello  ";
        
        //when
        String actualResult = hello.helloYou(input);
        
        //then
        assertEquals(expectedResult, actualResult);
        
    }
    
    // below Hamcrest tests:
    
    
    
}
